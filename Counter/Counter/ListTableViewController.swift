//
//  ListTableViewController.swift
//  Counter
//
//  Created by Logan McKinzie on 6/19/17.
//  Copyright © 2017 Logan McKinzie. All rights reserved.
//

import UIKit

class ListTableViewController: UITableViewController {
    
    var listArray: [CountList] = []
    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var navigationBar: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 66
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addAction(_ sender: UIBarButtonItem) {
        
        let alert = UIAlertController(title: "New List", message: "Enter Name", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:{ (alertAction:UIAlertAction!) in
            let textf = alert.textFields?[0]
            var listName = (textf?.text ?? "New List")
            
            if listName.isEmpty {
                listName = "New List"
            }
            
            self.listArray.insert(CountList(name: listName, count: 0), at: 0)
            self.insertRow()
            
        }))
        alert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "New List"
            textField.isSecureTextEntry = false
        })
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func insertRow() {
        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        tableView.endUpdates()
    }
    
    @IBAction func editAction(_: UIBarButtonItem) {
        if tableView.isEditing {
            tableView.beginUpdates()
            tableView.setEditing(false, animated: true)
            tableView.endUpdates()
            
            switchButtonStyle(nowEditing: false)
        } else {
            tableView.beginUpdates()
            tableView.setEditing(true, animated: true)
            tableView.endUpdates()
            
            switchButtonStyle(nowEditing: true)
        }
    }
    
    func switchButtonStyle(nowEditing: Bool) {
        if nowEditing {
            let newButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(editAction))
            navigationBar.leftBarButtonItem = newButton
     //       navigationBar.leftBarButtonItem?.action = #selector(ListTableViewController.editAction(_:))
        } else {
            let newButton = UIBarButtonItem.init(barButtonSystemItem: .edit, target: self, action: #selector(editAction))
            navigationBar.leftBarButtonItem = newButton
     //       navigationBar.leftBarButtonItem?.action = #selector(ListTableViewController.editAction(_:))
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> ListTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath) as! ListTableViewCell
        
        // Configure the cell...
        cell.cellName.text = "\(listArray[indexPath.row].name)"
        cell.cellCount.text = "\(listArray[indexPath.row].count) Items"
        
        return cell
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            listArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        
        let element = listArray.remove(at: fromIndexPath.row)
        listArray.insert(element, at: to.row)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toCount", sender: Any?.self)
    }
    
    // Override to support conditional rearranging of the table view.
//    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
//        // Return false if you do not want the item to be re-orderable.
//        return true
//    }
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
        var dvc = segue.destination as! CountTableViewController
     // Pass the selected object to the new view controller.
     }
    
}
