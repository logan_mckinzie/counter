//
//  CountList.swift
//  Counter
//
//  Created by Logan McKinzie on 6/19/17.
//  Copyright © 2017 Logan McKinzie. All rights reserved.
//

import Foundation

class CountList {
    
    var name: String
    var count: Int
    
    init(name: String, count: Int) {
        self.name = name
        self.count = count
    }
}
